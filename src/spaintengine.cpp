#include "spaintengine.h"

SPaintEngine::SPaintEngine(SWindow& window) :
    m_window(window)
{ }

SPaintEngine::~SPaintEngine()
{ }

void SPaintEngine::fillBack(const SColor& color)
{

}

void SPaintEngine::drawLine(const SPoint& p1, const SPoint& p2, const SColor& color)
{

}

void SPaintEngine::fillRect(const SPoint& p1, const SPoint& p2, const SColor& color)
{

}

void SPaintEngine::fillCircle(const SPoint& origin, unsigned int radius, const SColor& color)
{

}
