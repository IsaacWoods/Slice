#ifndef SOPENGLWINDOW_H
#define SOPENGLWINDOW_H

#include <iostream>
#include <string>
#include <GL/glew.h>
#include <GL/glfw.h>
#include <swindow.h>

class SOpenGLWindow : public SWindow
{
    public:
        SOpenGLWindow(int width, int height, const std::string& title) : SWindow(width, height, title) { }
        ~SOpenGLWindow();

        void create();
        void update();
};

#endif // SOPENGLWINDOW_H
