#ifndef UTIL_H
#define UTIL_H

class SColor
{
    public:
        SColor(unsigned int r, unsigned int g, unsigned int b, unsigned int a)
        {
            this->r = r;
            this->g = g;
            this->b = b;
            this->a = a;
        }

        unsigned int r, g, b, a;
};

class SPoint
{
    public:
        SPoint() : x(0.0f), y(0.0f) { }
        SPoint(float x, float y)
        {
            this->x = x;
            this->y = y;
        }

        float x, y;
};

#endif // UTIL_H
