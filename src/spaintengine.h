#ifndef SPAINTENGINE_H
#define SPAINTENGINE_H

#include <util.h>
#include <swindow.h>

class SPaintEngine
{
    public:
        SPaintEngine(SWindow& window);
        ~SPaintEngine();

        void fillBack(const SColor& color);
        void drawLine(const SPoint& p1, const SPoint& p2, const SColor& color);
        void fillRect(const SPoint& p1, const SPoint& p2, const SColor& color);
        void fillCircle(const SPoint& origin, unsigned int radius, const SColor& color);
    private:
        SWindow& m_window;
};

#endif // SPAINTENGINE_H
