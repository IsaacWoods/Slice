#include "sopenglwindow.h"

SOpenGLWindow::~SOpenGLWindow()
{
    glfwTerminate();
}

void SOpenGLWindow::create()
{
    if (!glfwInit())
    {
        std::cerr << "Failed to initialise GLFW!" << std::endl;
        return;
    }

    if (!glfwOpenWindow(m_width, m_height, 0, 0, 0, 0, 0, 0, GLFW_WINDOW))
    {
        std::cerr << "Failed to create GLFW window!" << std::endl;
        return;
    }

    glfwSetWindowTitle(m_title.c_str());
    glfwSwapInterval(0);

    if (glewInit() != GLEW_OK)
    {
        std::cerr << "Could not initialise GLEW!" << std::endl;
        return;
    }

    std::cout << "Running on OpenGL " << glGetString(GL_VERSION) << std::endl;
}

void SOpenGLWindow::update()
{
    // TODO: check for the window's closing signal and queue a closing event
    /*
    !glfwGetWindowParam(GLFW_OPENED);
    */

    glfwPollEvents();
    glfwSwapBuffers();
}
