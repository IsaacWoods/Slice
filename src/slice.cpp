#include "slice.h"
#include <thread>
#include <chrono>

#define SLEEP_DURATION 500

Slice::Slice(SliceContextType contextType) :
    paintEngine(*window)
{
    this->contextType = contextType;

    switch (contextType)
    {
        case SLICE_OPENGL:
            window = new SOpenGLWindow(800, 600, "Slice");
            window->create();
            break;
        case SLICE_INVALID:
            std::cerr << "Tried to create Slice context of invalid type!" << std::endl;
            PAUSE_CONFIRM;
    }
}

Slice::~Slice()
{
    delete window;
}

void Slice::sleep()
{
    std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_DURATION));
}
