#ifndef SWINDOW_H
#define SWINDOW_H

#include <string>

class SWindow
{
    public:
        SWindow(int width = 800, int height = 600, const std::string& title = "Slice");
        virtual ~SWindow() {}

        virtual void create() = 0;
        virtual void update() = 0;
    protected:
        int m_width, m_height;
        int m_xPos, m_yPos;
        std::string m_title;
};

#endif // SWINDOW_H
