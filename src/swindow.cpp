#include "swindow.h"
#include <slice.h>

SWindow::SWindow(int width, int height, const std::string& title) :
    m_width(width),
    m_height(height),
    m_title(title)
{ }

void SWindow::create()
{
    std::cerr << "Slice window manager didn't correctly create the window!" << std::endl;
    PAUSE_CONFIRM;
}
