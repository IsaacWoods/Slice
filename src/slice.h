#ifndef SLICE_H
#define SLICE_H

// SLICE_VERSION is (major << 16)(minor << 8)(patch)
// Check SLICE_VERSION with: #if (SLICE_VERSION >= CHECK_VERSION(3, 2, 5))
#define SLICE_VERSION 0x000001
#define SLICE_VERSION_STR "0.0.1"
#define CHECK_VERSION(major, minor, patch) ((major << 16)|(minor << 8)|(patch))

#define PAUSE_CONFIRM std::cin.get()

// --- Includes ---
#include <iostream>
#include <string>

// --- Project Includes ---
#include <swindow.h>
#include <sopenglwindow.h>
#include <spaintengine.h>
#include <util.h>

enum SliceContextType { SLICE_OPENGL, SLICE_INVALID };

class Slice
{
    public:
        Slice(SliceContextType contextType);
        ~Slice();

        void sleep();

        SliceContextType contextType;
        SWindow* window;
        SPaintEngine paintEngine;
};

#endif // SLICE_H
