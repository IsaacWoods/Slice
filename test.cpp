#include <iostream>
#include <slice.h>

int main()
{
    std::cout << "Hello, World!" << std::endl;

    Slice slice(SLICE_OPENGL);

    while (true)
    {
        slice.window->update();
        slice.sleep();
    }

    return 0;
}
