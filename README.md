# Slice
Slice is a highly-customizable, fast, responsive application framework. It is designed so beautiful, customized, fast UIs can be built using a more
design-centred approach, taking inspirations from the web-technologies. It uses an OpenGL back-end for rendering, so a vast number of visual effects and
customized behaviours are possible.

### Build Dependencies
* C++0x supporting compiler
* OpenGL
* GLFW
* GLEW
